#include <stdio.h>
#include <iostream>

int main()
{
  int i, j, N = 13;
  int c = N / 2;
  for(i = 0; i < N; i++)
  {
    for(j = 0; j < N; j++)
    {
      if(i <= c)
      {
        if(j >= c - i && j <= c + i)
          printf ("*");
        else
          printf (" ");
      }
      else
      {
          if(j >= c + i - N + 1 && j <= c - i + N - 1)
          printf ("*");
        else
          printf (" ");
      }
    }
    printf("\n");
  }
  
  return 0;
}